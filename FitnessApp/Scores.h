//
//  Scores.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface Scores : UIViewController<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>
@property (retain, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (retain, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *result;

- (void)saveContext;
- (IBAction)AddButton:(id)sender;

@end
