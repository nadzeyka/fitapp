//
//  FitnessSingleton.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/13/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "FitnessSingleton.h"

@implementation FitnessSingleton
@synthesize fitCompMenu;
//@synthesize managedObjectContext;
static FitnessSingleton * sharedFitnessSingleton = nil;

+ (FitnessSingleton*) sharedInstance
{
    if (sharedFitnessSingleton == nil)
    {
        sharedFitnessSingleton = [[super allocWithZone:NULL] init];
    }
    return sharedFitnessSingleton;
}
@end
