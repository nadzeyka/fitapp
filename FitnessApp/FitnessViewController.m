//
//  FitnessViewController.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "FitnessViewController.h"
#import "FitCategoryCell.h"
#import "Fit.h"
#import "StartView.h"
#import "Workout.h"
#import "AppDelegate.h"
@interface FitnessViewController ()

@end

@implementation FitnessViewController
@synthesize counterLabel;
@synthesize counterValue;
@synthesize timer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"HOME", @"HOME");
        self.tabBarItem.image = [UIImage imageNamed:@"29-heart"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"CHOOSE YOUR WORKOUT";
    UIBarButtonItem *backItem = [[[UIBarButtonItem alloc] initWithTitle:@" "
                                                                 style:UIBarButtonItemStyleBordered
                                                                target:nil
                                                                action:nil]autorelease];
    
    [self.navigationItem setBackBarButtonItem:backItem];
    
    // Do any additional setup after loading the view from its nib.
    self.dataArray = [NSMutableArray array];
    
    [_dataArray addObject:[Fit fitWithHeader:@"Killer Legs" subText:@"Workouts to tone your legs and butt" ]];
    [_dataArray addObject:[Fit fitWithHeader:@"Perfect Abs" subText:@"Workouts to sculpt your stomach" ]];
    [_dataArray addObject:[Fit fitWithHeader:@"Celebrity Shape" subText:@"Workouts to get you celebrity's body" ]];
    self.fitTable.scrollEnabled = NO;
    counterValue = 0;
	self.counterLabel.text = [NSString stringWithFormat:@"%d", counterValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FitCategoryCell *cell=(FitCategoryCell *)[tableView dequeueReusableCellWithIdentifier:@"FitCategoryCell"];
    if (cell == nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"FitCategoryCell" owner:nil options:nil]objectAtIndex:0];
    }
    Fit *f;
    
   
    f=_dataArray[indexPath.row];
    cell.headerText.text=f.headerText;
    cell.subText.text=f.subText;
    return cell;
}

- (void)updateLabel:(id)sender{
	if(counterValue >= MAX_VALUE){
		[self killTimer];
	}
	
	self.counterLabel.text = [NSString stringWithFormat:@"%d", counterValue];
	counterValue++;
}

- (IBAction)startWorkout:(id)sender {
     Workout *vc = [[[Workout alloc]initWithNibName:@"Workout" bundle:nil]autorelease];
    [self.navigationController pushViewController:vc animated:YES];


}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StartView *viewcontroller=[[[StartView alloc]init]autorelease];
    viewcontroller.numberOfCellComplex=indexPath.row;
    [self.navigationController pushViewController:viewcontroller animated:YES];
    
    }
  

- (IBAction)startTimer:(id)sender{
	self.counterValue = 0;
	
	[self killTimer];
	timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(updateLabel:)
                                           userInfo:nil 
                                            repeats:YES ];
	
	[self updateLabel:timer];
}
- (void)killTimer{
	if(timer){
		[timer invalidate];
		timer = nil;
	}
}
- (void)viewDidUnload {
    [super viewDidUnload];
	[self killTimer];
	counterLabel = nil;
}

- (void)dealloc {
    [_fitTable release];
    [_dataArray release];
    [super dealloc];    
}
@end
