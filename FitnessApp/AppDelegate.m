//
//  AppDelegate.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "AppDelegate.h"
#import "FitnessViewController.h"
#import "Workout.h"
#import "Scores.h"
#import "Settings.h"
#import "LogTable.h"


@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (void)dealloc
{   [_window release];
    [_fitnessVC release];
    [_fitnessNC release];
    [_workoutNC release];
    [_workoutVC release];
    [_scoresNC release];
    [_scoresVC release];
    [_settingsNC release];
    [_settingsVC release];
    [_fitnessTabbar release];
    [super dealloc];
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    UITabBarController *fitnessTabbar = [[[UITabBarController alloc]init]autorelease];
    FitnessViewController *fitnessVC = [[[FitnessViewController alloc]initWithNibName:@"FitnessViewController" bundle:nil]autorelease];
    Workout *workoutVC= [[[Workout alloc]initWithNibName:@"Workout" bundle:nil]autorelease];
    Scores *scoresVC = [[[Scores alloc]initWithNibName:@"Scores" bundle:nil]autorelease];
    scoresVC.managedObjectContext= self.managedObjectContext;

    Settings *settingsVC = [[[Settings alloc]initWithNibName:@"Settings" bundle:nil]autorelease];

    
    UINavigationController *fitnessNC = [[[UINavigationController alloc]initWithRootViewController:fitnessVC]autorelease];
    UINavigationController *workoutNC = [[[UINavigationController alloc]initWithRootViewController:workoutVC]autorelease];
    UINavigationController *scoresNC = [[[UINavigationController alloc]initWithRootViewController:scoresVC]autorelease];
    UINavigationController *settingsNC = [[[UINavigationController alloc]initWithRootViewController:settingsVC]autorelease];
    
    fitnessNC.title = @"Home";
    workoutNC.title = @"Workout";
    scoresNC.title = @"Scores";
    settingsNC.title = @"Settings";

    NSArray *array = [NSArray arrayWithObjects: fitnessNC,workoutVC,scoresVC,settingsVC,nil];
    fitnessTabbar.viewControllers = array;
    self.window.rootViewController = fitnessTabbar;
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;

}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];

}
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
  //  [FitnessSingleton sharedInstance].managedObjectContext=self.managedObjectContext;
    return _managedObjectContext;
}


// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"New" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"New.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil
                                                             URL:storeURL options:options error:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
