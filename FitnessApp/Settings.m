//
//  Settings.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "Settings.h"

//#import "APPChildViewController.h"

@interface Settings ()

@end

@implementation Settings
@synthesize scrollView;
@synthesize pageControl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"SETTINGS", @"SETTINGS");
       // self.tabBarItem.image = [UIImage imageNamed:@"Icon-small"];
    }
    return self;
}

- (void)viewDidLoad
{
    /*self.pageController = [[[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil]autorelease];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    APPChildViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
        [self.pageController didMoveToParentViewController:self];*/
    // Do any additional setup after loading the view from its nib.
    CGFloat currentX = 0.0f;
    NSArray *tipsPics = [NSArray arrayWithObjects:
                        [UIImage imageNamed:@"tip1.png"],
                        [UIImage imageNamed:@"tip2.png"],
                        [UIImage imageNamed:@"tip3.png"],
                        [UIImage imageNamed:@"tip4.png"],
                        [UIImage imageNamed:@"tip5.png"],
                        nil] ;
    scrollView = [[UIScrollView alloc]
                       initWithFrame:CGRectMake(0, 0,
                                                self.view.frame.size.width,
                                                self.view.frame.size.height)];
    scrollView.contentSize = CGSizeMake(320*5,404);
    scrollView.pagingEnabled=YES;
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    for (int i=0; i <tipsPics.count; i++)
    {
        UIImage *images = [tipsPics objectAtIndex:i];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:images];
        // put image on correct position
        CGRect rect = imageView.frame;
        rect.origin.x = currentX;
        imageView.frame = rect;
        // update currentX
        currentX += imageView.frame.size.width;
        [scrollView addSubview:imageView];
        [imageView release]; }
    [self.view addSubview:scrollView];
    pageControl = [[[UIPageControl alloc] init] autorelease];
    pageControl.frame = CGRectMake(0,0,320,50);
    pageControl.numberOfPages = tipsPics.count;
    pageControl.currentPage = 0;
    
    [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
   
    [self.view addSubview:self.pageControl];
    pageControl.backgroundColor = [UIColor blackColor];
    [scrollView release];
    [super viewDidLoad];
    
   }

- (IBAction)changePage:(id)sender {
    CGFloat x = pageControl.currentPage * scrollView.frame.size.width;
    [scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*- (APPChildViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    APPChildViewController *childViewController = [[[APPChildViewController alloc] initWithNibName:@"APPChildViewController" bundle:nil]autorelease];
    childViewController.index = index;
    return childViewController;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)settingsVC {
    
    NSUInteger index = [(APPChildViewController *)settingsVC index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)settingsVC {
    
    NSUInteger index = [(APPChildViewController *)settingsVC index];
    
    index++;
    
    if (index == 5) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.

    return 5;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width; // you need to have a **iVar** with getter for scrollView
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = fractionalPage;
    self.pageControl.currentPage = page; // you need to have a **iVar** with getter for pageControl

}
- (void)dealloc {
    [super dealloc];
   [scrollView release];
}

@end
