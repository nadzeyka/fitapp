//
//  Workout.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "Workout.h"
#import "Exercise.h"
#import "FitnessSingleton.h"
#import "LogTable.h"
#import "Scores.h"
#import <CoreData/CoreData.h>

@interface Workout ()

@end

@implementation Workout
@synthesize timer;
@synthesize indx;
@synthesize fitLog;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"WORKOUT", @"WORKOUT");
        self.tabBarItem.image = [UIImage imageNamed:@"13-target"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    switch([FitnessSingleton sharedInstance].fitCompMenu){
        case KillerLegsItem:
            self.imgsArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"KLSLBK.png"],
                              [UIImage imageNamed:@"KLTT.png"],[UIImage imageNamed:@"KLCC.png"],
                              [UIImage imageNamed:@"KLGS.png"],nil];
            _myArray = [[NSMutableArray alloc] initWithObjects:
                        @"Stand with feet hip-width apart, arms by sides.\nLunge to right with right leg,keeping toes pointed forward and bending right knee 90 dgrs.\nHinge forward from hips and touch left hand to floor.",
                        @"Stand with feet shoulder-width apart,arms by sides.\nBend knees and hinge forward from hips to touch hands to floor a few inc in front of feet.\nLift left foot behind you a few inc off floor and point left.",
                        @"Stand with feet shoulder-width apart,elbows bent by sides, hands in front of chest.\nLunge back with left leg, bringing left foot toward right and bending both knees 90 dgrs into a curtsy;swing bent left.",
                        @"Stand with feet wider than shoulder-width apart,toes turned out slightly,elbows bent with hands in front of you\nLower into squat.\nReturn to standing,balancing on left leg as you swing.", nil];
            break;
        case WorkoutsItem:
            self.imgsArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"images-4.jpeg"],
                              [UIImage imageNamed:@"images-5.jpeg"],[UIImage imageNamed:@"images-6.jpeg"],
                              [UIImage imageNamed:@"images-7.jpeg"],nil];
            _myArray = [[NSMutableArray alloc] initWithObjects: @"string2", @"string3", @"string4", @"string5", nil];
            break;
        case PerfectAbs:
            self.imgsArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"KLSLBK.png"],
                              [UIImage imageNamed:@"KLTT.png"],[UIImage imageNamed:@"KLCC.png"],
                              [UIImage imageNamed:@"KLGS.png"],nil];
            _myArray = [[NSMutableArray alloc] initWithObjects: @"string2", @"string3", @"string4", @"string5", nil];
            break;
    }
    // Do any additional setup after loading the view from its nib.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)updateText:(NSTimer *)theTimer
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:2];
   if (indx < _myArray.count)
    {
        _text.text = [self.myArray objectAtIndex:indx];
        self.imageView.image = [self.imgsArray objectAtIndex:indx];
        indx++;
        [UIView commitAnimations];
    }
    else
    {
        indx = 0;
        [timer invalidate];
    }
    [UIView commitAnimations];
}

- (void)dealloc {
    [_imageView release];
    [_text release];
    [super dealloc];
}

- (IBAction)startWorkout:(id)sender {
    self.imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(20, 65, 280,300)]autorelease];
    [self.view addSubview:self.imageView];
    _imageView.image =[UIImage imageNamed:@"KLDL.png"];
    _text.text = @"Stand with feet hip-width apart, arms by sides.\nLunge to right with right leg,keeping toes pointed forward and bending right knee 90 dgrs.\n Hinge forward from hips and touch left hand to floor.";
    timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateText:) userInfo:nil repeats:YES];
    }

    @end
