//
//  Exercise.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "Exercise.h"
#import "FitnessSingleton.h"
@interface Exercise ()

@end

@implementation Exercise
@synthesize numberOfCellExercise;
@synthesize exercisePicsArray;
@synthesize exerciseDescriptionArray;
@synthesize exerciseImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    exercisePicsArray = [[NSMutableArray alloc] init];
    switch([FitnessSingleton sharedInstance].fitCompMenu){
        case KillerLegsItem:
            [exercisePicsArray addObject:[UIImage imageNamed:@"KLDL.png"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"KLSLBK.png"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"KLTT.png"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"KLCC.png"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"KLGS.png"]];
            exerciseDescriptionArray = [[NSMutableArray alloc] initWithObjects:
                        @"Stand with feet hip-width apart,arms by sides.Lunge to right with right leg,keeping toes pointed forward and bending right knee 90 dgrs.Hinge forward from hips and touch left hand to floor.",
                        @"Stand with feet hip-width apart, arms by sides.Lunge to right with right leg,keeping toes pointed forward and bending right knee 90 dgrs.Hinge forward from hips and touch left hand to floor.",
                        @"Stand with feet shoulder-width apart,arms by sides.Bend knees and hinge forward from hips to touch hands to floor a few inc in front of feet.Lift left foot behind you a few inc off floor and point left.",
                        @"Stand with feet shoulder-width apart,elbows bent by sides,hands in front of chest.Lunge back with left leg,bringing left foot toward right and bending both knees 90 dgrs into a curtsy;swing bent left.",
                        @"Stand with feet wider than shoulder-width apart,toes turned out slightly,elbows bent with hands in front of you.Lower into squat.Return to standing,balancing on left leg as you swing.", nil];
            break;
        case WorkoutsItem:
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-7.jpeg"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-7.jpeg"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-7.jpeg"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-7.jpeg"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-7.jpeg"]];
            break;
        case PerfectAbs:
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-3.jpeg"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-3.jpeg"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-3.jpeg"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-6.jpeg"]];
            [exercisePicsArray addObject:[UIImage imageNamed:@"images-7.jpeg"]];
            break;
    }
   
    for (int i = 0; i <=numberOfCellExercise; i++)
    {
        exerciseImage.image=[exercisePicsArray objectAtIndex:i];
        _text.text = [self.exerciseDescriptionArray objectAtIndex:i];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [exerciseImage release];
    [_text release];
    [super dealloc];
}
@end
