//
//  StartView.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/11/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "StartView.h"
#import "Exercise.h"
#import "FitnessSingleton.h"

@interface StartView ()
@end

@implementation StartView
@synthesize numberOfCellComplex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title=@"Exercises";
    switch (self.numberOfCellComplex) {
        case 0:
            [FitnessSingleton sharedInstance].fitCompMenu=KillerLegsItem;
            _exerciseArray = [NSMutableArray arrayWithObjects:@"Diagonal Leg",@"Side-Lunge Butt Kicker",@"Thigh Trimmer",@"Carving Curtsy",@"Golf Swing",nil];
            break;
        case 1:
            [FitnessSingleton sharedInstance].fitCompMenu=WorkoutsItem;

           _exerciseArray = [NSMutableArray arrayWithObjects:@"Bent Knee Hip Raises",@"Cross Body Crunches",@"Basic Crunch",@"Right Oblique Crunch",@"Left Oblique Crunch",nil];
         break;
        case 2:
            [FitnessSingleton sharedInstance].fitCompMenu=PerfectAbs;

          _exerciseArray = [NSMutableArray arrayWithObjects:@"Diagonal Leg",@"Side-Lunge Butt Kicker",@"Thigh Trimmer",@"Carving Curtsy",@"Golf Swing",nil];
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table View Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _exerciseArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    // Configure the cell.
    cell.textLabel.text = [_exerciseArray objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Exercise *viewcontroller=[[[Exercise alloc]init]autorelease];
    viewcontroller.numberOfCellExercise=indexPath.row;
    [self.navigationController pushViewController:viewcontroller animated:YES];
}
@end
