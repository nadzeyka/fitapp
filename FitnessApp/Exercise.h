//
//  Exercise.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Exercise : UIViewController
@property (retain, nonatomic) IBOutlet UITextView *text;
@property int numberOfCellExercise;
@property (nonatomic,retain) NSMutableArray *exercisePicsArray;
@property (nonatomic,retain) NSMutableArray *exerciseDescriptionArray;
@property (retain, nonatomic) IBOutlet UIImageView *exerciseImage;



@end
