//
//  Fit.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/10/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "Fit.h"

@implementation Fit
+ (id)fitWithHeader:(NSString *)headerText subText:(NSString *)subText
{
    Fit *f = [[[Fit alloc]init]autorelease];
    f.headerText = headerText;
    f.subText = subText;
    return f;
}
@end
