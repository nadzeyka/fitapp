//
//  FitnessViewController.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FitnessViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
};

#define MAX_VALUE 60
@property (assign, nonatomic) IBOutlet UILabel *counterLabel;
@property int counterValue;
@property (copy) NSTimer *timer;
@property (nonatomic,retain) NSMutableArray *rowarray;
@property (retain, nonatomic) IBOutlet UITableView *fitTable;

@property(nonatomic, retain)NSMutableArray *dataArray;
- (IBAction)startTimer:(id)sender;
- (void)killTimer;
- (void)updateLabel:(id)sender;
- (IBAction)startWorkout:(id)sender;

@end
