//
//  Scores.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "Scores.h"
#import "FitnessSingleton.h"
#import "LogTable.h"
#import "Workout.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface Scores ()
@end

@implementation Scores
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize managedObjectContext = _managedObjectContext;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"LOG", @"LOG");
       // self.tabBarItem.image = [UIImage imageNamed:@"log-Small"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title=@"CC";
    UIBarButtonItem *addButton = [[[UIBarButtonItem alloc] initWithTitle:@" 11"
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:@selector(insertNewObject)]autorelease];
    
    [self.navigationItem setRightBarButtonItem:addButton];
    self.navigationItem.rightBarButtonItem = addButton;
    AppDelegate* ad = [UIApplication sharedApplication].delegate;
    self.managedObjectContext = ad.managedObjectContext;
    [self testFetch];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table View Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier]autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    LogTable *myObj=self.result[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@  %@",myObj.dateWorkout,myObj.timeWorkout];
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCellEditingStyle style = UITableViewCellEditingStyleNone;

        if (indexPath.row == [self.result count]) {
            style = UITableViewCellEditingStyleInsert;
        }
        else {
            style = UITableViewCellEditingStyleDelete;
        }
    return style;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
        NSManagedObjectContext *context = self.managedObjectContext;
        NSManagedObject *eventToDelete = [self.result objectAtIndex:indexPath.row];
        [context deleteObject:eventToDelete];

        // Save the context.
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self testFetch];
        [self.tableView reloadData];;
    }
   

    
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return YES;
}


#pragma mark - Fetched results controller 
- (void)insertNewObject
{
    NSManagedObjectContext *context = self.managedObjectContext;
    NSEntityDescription *new = [NSEntityDescription entityForName:@"LogTable" inManagedObjectContext:context];
    LogTable *newObject = [[[LogTable alloc]initWithEntity:new insertIntoManagedObjectContext:context]autorelease];
    newObject.dateWorkout = [NSDate date];
    newObject.timeWorkout =[NSNumber numberWithInt:(int)12];
    [self saveContext];
    [self testFetch];
    [self.tableView reloadData];
}

- (void)saveContext {
    NSManagedObjectContext *context = self.managedObjectContext;
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}
-(void)testFetch {
    NSManagedObjectContext * managedObjectContext = self.managedObjectContext;
    NSEntityDescription *tabataWorkoutEntity = [NSEntityDescription entityForName:@"LogTable" inManagedObjectContext:managedObjectContext];
    NSError *error;
    NSFetchRequest *request = [[[NSFetchRequest alloc]init]autorelease];
    [request setEntity:tabataWorkoutEntity];
    [request setReturnsObjectsAsFaults:NO];
    _result = [[[NSMutableArray alloc]init]autorelease];
    NSSortDescriptor *dateSort = [[[NSSortDescriptor alloc] initWithKey:@"dateWorkout" ascending:NO]autorelease];
    [request setSortDescriptors:[NSArray arrayWithObject:dateSort]];
    NSArray *dataArray = (NSMutableArray *)[self.managedObjectContext executeFetchRequest:request error:&error];
    _result = [dataArray mutableCopy];
    NSLog(@"%d",self.result.count);
}

- (IBAction)AddButton:(id)sender {
    [self insertNewObject];
}
- (void)dealloc {
    [super dealloc];
    [_result release];
}

@end
