//
//  Settings.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Settings : UIViewController<UIPageViewControllerDataSource,UIScrollViewDelegate>
//@property (retain, nonatomic) UIPageViewController *pageController;

@property (retain,nonatomic) UIScrollView *scrollView;
@property (retain,nonatomic) UIPageControl *pageControl;
@end
