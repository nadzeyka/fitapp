//
//  Workout.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/7/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogTable.h"

@interface Workout : UIViewController <UITextViewDelegate>
{
}

@property (nonatomic,retain) NSArray *imgsArray;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) IBOutlet UITextView *text;
@property (nonatomic,retain)NSMutableArray *myArray;
@property (copy) NSTimer *timer;
@property int indx;
@property (retain,nonatomic)LogTable *fitLog;
@property(nonatomic,retain)NSMutableArray *dateWorkoutArray;

- (IBAction)startWorkout:(id)sender;

@end

