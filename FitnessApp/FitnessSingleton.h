//
//  FitnessSingleton.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/13/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
typedef enum {
    KillerLegsItem,
    WorkoutsItem,
    PerfectAbs
}fitnessComplex;


@interface FitnessSingleton : NSObject
@property fitnessComplex fitCompMenu;
+ (FitnessSingleton*) sharedInstance;
//@property (retain,nonatomic)NSManagedObjectContext *managedObjectContext;
@end
