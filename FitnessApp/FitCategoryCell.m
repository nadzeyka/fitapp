//
//  FitCategoryCell.m
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/10/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import "FitCategoryCell.h"

@implementation FitCategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_infoButton release];
    [super dealloc];
}
- (IBAction)infpButtonPushed:(id)sender {

}
@end
