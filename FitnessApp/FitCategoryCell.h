//
//  FitCategoryCell.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/10/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FitCategoryCell : UITableViewCell
@property (assign,nonatomic) IBOutlet UILabel *headerText;
@property (assign,nonatomic) IBOutlet UILabel *subText;
@property (retain, nonatomic) IBOutlet UIButton *infoButton;
- (IBAction)infpButtonPushed:(id)sender;

@end
