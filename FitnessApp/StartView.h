//
//  StartView.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/11/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartView : UIViewController
@property (nonatomic,retain) NSMutableArray *exerciseArray;
@property int numberOfCellComplex;
@end
