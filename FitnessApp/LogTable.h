//
//  LogTable.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/18/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LogTable : NSManagedObject

@property (nonatomic, retain) NSNumber * calloriesCount;
@property (nonatomic, retain) NSDate * dateWorkout;
@property (nonatomic, retain) NSNumber * timeWorkout;

@end
