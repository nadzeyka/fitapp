//
//  Fit.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 2/10/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fit : NSObject
@property (strong) NSString *headerText;
@property (strong) NSString *subText;

+ (id)fitWithHeader:(NSString *)headerText subText:(NSString *)subText;
@end
