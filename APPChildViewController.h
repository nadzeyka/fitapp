//
//  APPChildViewController.h
//  FitnessApp
//
//  Created by Nadzeya Karaban on 3/19/14.
//  Copyright (c) 2014 Nadzeya Karaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APPChildViewController : UIViewController
@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UILabel *screenNumber;
@end
